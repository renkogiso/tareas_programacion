struct auto
{
  int anyo;//año
  char marca[20];
  int placas;

};

struct computadora
{
  char marca[20];
  float pulgadas;
  int memoria_ram;
  int velocidad;
  int precio;

};

struct alumno
{
  int edad;
  char nombre[30];
  int noCuenta;
  int grupo;
  char carrera[30];
  char horario[11];

};

struct equipoDeFutbol
{
  char nombreEquipo[30];
  int noJugadores;
  int noVictorias;
  int noDerrotas;
};

struct mascota
{
  char raza[20];
  int edad;
  char color[15];
  char personalidad[50];

};

struct videoJuego
{
  char nombre[30];
  int precio;
  int noJugadores;
  char modoDeJuego[20];

};

struct superHeroe
{
  char nombre[50];
  int edad;
  char poderes[60];

};

struct panetas
{
  int noPlanetas;
  char nombreCadaPlaneta[150];
  float distaciaHaciaTierra;
  int gravedad;
  float tamanyo;
};

struct personajeDeSuJuegoFavorito
{
  char nombre[50];
  char genero[10];
  char nombreJuego[50];

};

struct comic
{
  char nombre[50];
  int volumenes;
  char nombrePersonajes;
}
