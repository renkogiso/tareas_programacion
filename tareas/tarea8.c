#include <stdio.h>
int main(int argc, char const *argv[]) {
  printf("\nel tamaño en bytes de un int es: %lu\n",sizeof(int));
  printf("el tamaño en bytes de un float es: %lu\n",sizeof(float));
  printf("el tamaño en bytes de un char es: %lu\n",sizeof(char));
  printf("el tamaño en bytes de un long int es: %lu\n\n",sizeof(long int));
  return 0;
}
